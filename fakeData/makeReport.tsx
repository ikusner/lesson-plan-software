import dayjs from "dayjs";
import Link from "next/link";

const makeDF = data => {
  if (data.timesUsed === 0) {
    return "None";
  }

  return (
    <div>
      <div>{data.timesUsed}</div>
      <Link passHref href={`/myplans/${data.lastUsed}`}>
        {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
        <a style={{ color: "inherit" }} target="_blank" rel="noopener noreferrer">
          {dayjs(new Date()).format("MM/DD/YY")}
        </a>
      </Link>
    </div>
  );
};

const reportData = data => ({
  standard: data.name,
  freq1: makeDF(data.dok1),
  freq2: makeDF(data.dok2),
  freq3: makeDF(data.dok3),
  freq4: makeDF(data.dok4),
  mins1: data.dok1.time,
  mins2: data.dok2.time,
  mins3: data.dok3.time,
  mins4: data.dok4.time
});

const makeReportData = data => {
  console.log("reportData", data);
  const makeDataLevel = data.map(el => reportData(el));

  return makeDataLevel;
};

export default makeReportData;
