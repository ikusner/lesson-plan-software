const range = len => {
  const arr = [];
  for (let i = 0; i < len; i++) {
    arr.push(i);
  }
  return arr;
};

const newPerson = () => ({
  subject: "Social Studies",
  grade: "K",
  period: "1",
  time: 30,
  open_default: 10,
  close_default: 10
});

export default function makeProfileData(data: any[]) {
  const makeDataLevel = () => {
    data.map(el => ({
      ...el
    }));
  };

  return makeDataLevel();
}
