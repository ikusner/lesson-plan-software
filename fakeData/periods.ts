interface Period {
  period: string;
  grade: string;
  subject: string;
  time: number;
  open_default: number;
  close_default: number;
}

const periods: Period[] = [
  {
    period: "1",
    grade: "4",
    subject: "Math",
    time: 30,
    open_default: 15,
    close_default: 15
  },
  {
    period: "2",
    grade: "3",
    subject: "Math",
    time: 30,
    open_default: 15,
    close_default: 15
  },
  {
    period: "3",
    grade: "4",
    subject: "Science",
    time: 30,
    open_default: 15,
    close_default: 15
  }
];

interface Dropdown {
  grade: any;
  value: string;
  label: string;
  description: string;
  obj?: {};
}

const PeriodDropdown: Dropdown[] = periods.map(period => ({
  value: JSON.stringify(period),
  label: `${period.grade} ${period.subject}`,
  grade: period.grade,
  subject: period.subject,
  description: `time:${period.time} open:${period.open_default} close:${period.close_default}`,
  obj: period
}));

const makePeriodDropdown = periodsFound =>
  periodsFound?.map(_p => ({
    value: _p.period,
    label: `${_p.grade} ${_p.subject}`,
    grade: _p.grade,
    subject: _p.subject,
    description: `time:${_p.time} open:${_p.open_default} close:${_p.close_default}`
  }));

export { makePeriodDropdown, PeriodDropdown, periods };
