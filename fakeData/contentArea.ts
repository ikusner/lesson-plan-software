const contentArea = {
  K_ENGLISH: ["LITERATURE", "INFO TEXT", "Foundational", "WRITING", "SPK & LIST", "LANG"],
  "1_ENGLISH": ["LITERATURE", "INFO TEXT", "Foundational", "WRITING", "SPK & LIST", "LANG"],
  "2_ENGLISH": ["LITERATURE", "INFO TEXT", "Foundational", "WRITING", "SPK & LIST", "LANG"],
  "3_ENGLISH": ["LITERATURE", "INFO TEXT", "Foundational", "WRITING", "SPK & LIST", "LANG"],
  "4_ENGLISH": ["LITERATURE", "INFO TEXT", "Foundational", "WRITING", "SPK & LIST", "LANG"],
  "5_ENGLISH": ["LITERATURE", "INFO TEXT", "Foundational", "WRITING", "SPK & LIST", "LANG"],
  "6_ENGLISH": ["LITERATURE", "INFO TEXT", "WRITING", "SPK & LIST", "LANG"],
  "7_ENGLISH": ["LITERATURE", "INFO TEXT", "WRITING", "SPK & LIST", "LANG"],
  "8_ENGLISH": ["LITERATURE", "INFO TEXT", "WRITING", "SPK & LIST", "LANG"],
  HS_ENGLISH: ["LITERATURE", "INFO TEXT", "WRITING", "SPK & LIST", "LANG"],
  ANCHOR_ENGLISH: ["READ", "WRITING", "SPK & LIST", "LANG"],
  K_HEALTH: [],
  "1_HEALTH": [],
  "2_HEALTH": [],
  "3_HEALTH": [],
  "4_HEALTH": [],
  "5_HEALTH": [],
  "6_HEALTH": [],
  "7_HEALTH": [],
  "8_HEALTH": [],
  HS_HEALTH: [],
  K_MATH: ["COUNT", "OP & ALG", "NUM & BASE", "MSR & DATA", "GEOM"],
  "1_MATH": ["OP & ALG", "NUM & BASE", "MSR & DATA", "GEOM"],
  "2_MATH": ["OP & ALG", "NUM & BASE", "MSR & DATA", "GEOM"],
  "3_MATH": ["OP & ALG", "NUM & BASE", "NUM & FRAC", "MSR & DATA", "GEOM"],
  "4_MATH": ["OP & ALG", "NUM & BASE", "NUM & FRAC", "MSR & DATA", "GEOM"],
  "5_MATH": ["OP & ALG", "NUM & BASE", "NUM & FRAC", "MSR & DATA", "GEOM"],
  "6_MATH": ["RATIO", "NUM SYS", "EXP & EQ", "GEOM", "STAT & PROB"],
  "7_MATH": ["RATIO", "NUM SYS", "EXP & EQ", "GEOM", "STAT & PROB"],
  "8_MATH": ["NUM SYS", "EXP & EQ", "FUNC", "GEOM", "STAT & PROB"],
  HS_MATH: ["NUM SYS", "QUANT", "VECT & MAT", "EXP & EQ", "POLY", "FUNC", "LIN & EXP", "GEOM", "STAT & PROB"],
  "K_PHYS ED": [],
  "1_PHYS ED": [],
  "2_PHYS ED": [],
  "3_PHYS ED": [],
  "4_PHYS ED": [],
  "5_PHYS ED": [],
  "6_PHYS ED": [],
  "7_PHYS ED": [],
  "8_PHYS ED": [],
  "HS_PHYS ED": [],
  K_SCIENCE: ["EARTH", "LIFE", "PHYSICAL"],
  "1_SCIENCE": ["EARTH", "LIFE", "PHYSICAL"],
  "2_SCIENCE": ["EARTH", "LIFE", "PHYSICAL"],
  "3_SCIENCE": ["EARTH", "LIFE", "PHYSICAL"],
  "4_SCIENCE": ["EARTH", "LIFE", "PHYSICAL"],
  "5_SCIENCE": ["EARTH", "LIFE", "PHYSICAL"],
  "6_SCIENCE": ["EARTH", "LIFE", "PHYSICAL"],
  "7_SCIENCE": ["EARTH", "LIFE", "PHYSICAL"],
  "8_SCIENCE": ["EARTH", "LIFE", "PHYSICAL"],
  HS_SCIENCE: ["PHYSICAL", "LIFE", "EARTH"],
  "K_SOC ST": ["HIST", "GEO", "GOVT", "ECON"],
  "1_SOC ST": ["HIST", "GEO", "GOVT", "ECON"],
  "2_SOC ST": ["HIST", "GEO", "GOVT", "ECON"],
  "3_SOC ST": ["HIST", "GEO", "GOVT", "ECON"],
  "4_SOC ST": ["HIST", "GEO", "GOVT", "ECON"],
  "5_SOC ST": ["HIST", "GEO", "GOVT", "ECON"],
  "6_SOC ST": ["HIST", "GEO", "GOVT", "ECON"],
  "7_SOC ST": ["HIST", "GEO", "GOVT", "ECON"],
  "8_SOC ST": ["HIST", "GEO", "GOVT", "ECON"],
  "HS_SOC ST": ["HIST", "GOVT", "GEO", "ECON"]
};

export default contentArea;
