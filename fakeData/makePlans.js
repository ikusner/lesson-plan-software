const range = len => {
  const arr = [];
  for (let i = 0; i < len; i++) {
    arr.push(i);
  }
  return arr;
};

const newPerson = () => ({
  planDates: [],
  title: "title",
  rating: 1,
  subjects: []
});

const makeReportData = (...lens) => {
  const makeDataLevel = (depth = 0) => {
    const len = lens[depth];
    return range(len).map(d => ({
      ...newPerson()
    }));
  };

  return makeDataLevel();
};

export default makeReportData;
