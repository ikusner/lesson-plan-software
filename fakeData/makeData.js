const range = len => {
  const arr = [];
  for (let i = 0; i < len; i++) {
    arr.push(i);
  }
  return arr;
};

const newPerson = () => ({
  firstName:
    "Creatine supplementation is the reference compound for increasing muscular creatine levels; there is variability in this increase, however, with some nonresponders.",
  age: "10/27/21",
  visits: "10/27/21",
  progress: "10/17/21",
  status: "10/27/21",
  age1: "10/27/21",
  visits1: "10/27/21",
  progress1: "10/17/21",
  status1: "10/27/21"
});

const makeData = (...lens) => {
  console.log("make data");
  const makeDataLevel = (depth = 0) => {
    const len = lens[depth];
    return range(len).map(d => ({
      ...newPerson()
      // subRows: lens[depth + 1] ? makeDataLevel(depth + 1) : undefined,
    }));
  };

  return makeDataLevel();
};

export default makeData;
