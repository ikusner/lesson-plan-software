const range = len => {
  const arr = [];
  for (let i = 0; i < len; i++) {
    arr.push(i);
  }
  return arr;
};

const newPerson = () => ({
  subject: "Social Studies",
  grade: "K",
  period: "1",
  time: 30,
  open_default: 10,
  close_default: 10
});

const makeProfileData = (...lens) => {
  const makeDataLevel = (depth = 0) => {
    const len = lens[depth];
    return range(len).map(d => ({
      ...newPerson()
      // subRows: lens[depth + 1] ? makeDataLevel(depth + 1) : undefined,
    }));
  };

  return makeDataLevel();
};

export default makeProfileData;
