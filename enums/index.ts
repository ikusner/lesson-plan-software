export enum Grades {
  KINDERGARTEN = "K",
  FIRST = "1",
  SECOND = "2",
  THIRD = "3",
  FOURTH = "4",
  FIFTH = "5",
  SIXTH = "6",
  SEVENTH = "7",
  EIGHTH = "8",
  HIGH_SCHOOL = "HS"
}

export enum Subjects {
  COMPUTER_SCIENCE = "Computer Science",
  CONSUMER_SCIENCE = "Consumer Science",
  DRAMA = "Drama",
  ENGLISH = "English",
  FINANCIAL_LITERACY = "Financial Literacy",
  HANDWRITING = "Handwriting",
  HEALTH = "Health",
  MATH = "Math",
  MUSIC = "Music",
  PE = "PE",
  SCIENCE = "Science",
  SOCIAL_STUDIES = "Social Studies",
  TECHNOLOGY = "Technology",
  VISUAL_ARTS = "Visual Arts",
  WORLD_LANGUAGE = "World Language"
}
