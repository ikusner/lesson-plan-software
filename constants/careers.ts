const careers: string[] = [
  "Agriculture, Food & Natural Resources",
  "Architecture & Construction",
  "Arts, Audio/Video Technology & Communications",
  "Business, Management & Administration",
  "Education & Training",
  "Finance",
  "Government & Public Administration",
  "Health Science",
  "Hospitality & Tourism",
  "Human Services",
  "Information Technology",
  "Law, Public Safety, Corrections & Security",
  "Manufacturing",
  "Marketing, Sales & Service",
  "Science, Technology, Engineering & Mathematics",
  "Transportation, Distribution & Logistics"
];

interface Dropdown {
  label: string;
  value: string;
}

const careersDropdown: Dropdown[] = careers.map(career => ({
  label: career,
  value: career
}));

export default careersDropdown;
