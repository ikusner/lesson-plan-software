const subjects: string[] = [
  // "Computer Science",
  // "Consumer Science",
  // "Drama",
  "English",
  // "Financial Literacy",
  // "Handwriting",
  // "Health",
  "Math",
  // "Music",
  // "PE",
  "Science"
  // "Social Studies",
  // "Technology",
  // "Visual Arts",
  // "World Language",
];

interface Dropdown {
  label: string;
  value: string;
}

const subjectsDropdown: Array<Dropdown> = subjects.map(subject => ({
  label: subject,
  value: subject
}));

export default subjectsDropdown;
