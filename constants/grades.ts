const grades: string[] = ["K", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"];

interface Dropdown {
  label: string;
  value: string;
}

const gradesDropdown: Array<Dropdown> = grades.map(grade => ({
  label: grade,
  value: grade
}));

export default gradesDropdown;
