const opening: string[] = [
  "Bell Ringer",
  "Problem of the Day",
  "Reading",
  "Technology",
  "Writing",
  "Writing Que",
  "Other"
];
// BODY STRATEGIES
const body: string[] = [
  "Classroom Discussion",
  "Direct Instruction",
  "Jigsaw",
  "Microteaching",
  "Mnemonics",
  "Peer Tutoring",
  "Problem Solving Teaching",
  "Reciprocal Teaching",
  "Scaffolding",
  "Summarization",
  "Technology",
  "Other"
];

// CLOSING STRATEGIES
const closing: string[] = ["Discussion", "Exit Ticket", "Feedback", "Summarization", "Technology", "Other"];

interface Dropdown {
  label: string;
  value: string;
}

const openingDropdown: Dropdown[] = opening.map(o => ({
  label: o,
  value: o
}));
const bodyDropdown: Dropdown[] = body.map(b => ({
  label: b,
  value: b
}));
const closingDropdown: Dropdown[] = closing.map(c => ({
  label: c,
  value: c
}));

export { bodyDropdown, closingDropdown, openingDropdown };
