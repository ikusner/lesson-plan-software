// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getFirestore } from "firebase/firestore";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyD5SvZaKMjFfbsNHHpSND0_kO2mkB_0avs",
  authDomain: "structured-creativity.firebaseapp.com",
  projectId: "structured-creativity",
  storageBucket: "structured-creativity.appspot.com",
  messagingSenderId: "968785080131",
  appId: "1:968785080131:web:602eb13478caaf21fe09f6",
  measurementId: "G-9LE6Y0EY2Q"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

export const db = getFirestore(app);
export const auth = getAuth();
// const analytics = getAnalytics(app);
