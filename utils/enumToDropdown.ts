import { Grades } from "enums";

const enumToDropDown = (enumToConvert: any) => {
  Object.keys(Grades).map(e => ({
    value: Grades[e],
    label: Grades[e]
  }));
};

export default enumToDropDown;
