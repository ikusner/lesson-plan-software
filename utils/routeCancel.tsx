import { useRouter } from "next/router";
import * as React from "react";
import { FormState } from "react-hook-form";

type Props = {
  formState: FormState<any>;
  message?: string;
};

const defaultMessage = "Are you sure to leave without save?";

const useLeaveConfirm = ({ formState, message = defaultMessage }: Props) => {
  const Router = useRouter();

  // eslint-disable-next-line consistent-return
  const onRouteChangeStart = React.useCallback(() => {
    if (formState.isDirty) {
      if (window.confirm(message)) {
        return true;
      }
      // eslint-disable-next-line no-throw-literal
      throw "Abort route change by user's confirmation.";
    }
  }, [formState]);

  React.useEffect(() => {
    Router.events.on("routeChangeStart", onRouteChangeStart);

    return () => {
      Router.events.off("routeChangeStart", onRouteChangeStart);
    };
  }, [onRouteChangeStart]);

  // eslint-disable-next-line no-useless-return
  return;
};

export default useLeaveConfirm;
