import { Lesson, Period, User, UserStandards } from "../interfaces";
import standards from "../testStandards.json";

/** Dummy user data. */
export const sampleUserData: User[] = [
  { id: 101, name: "Alice", subjects: ["Math", "Science"], grade: "1" },
  { id: 102, name: "Bob", subjects: ["Math", "Science"], grade: "2" },
  { id: 103, name: "Caroline", subjects: ["Math", "Science"], grade: "K" },
  { id: 104, name: "Dave", subjects: ["Math", "Science"], grade: "HS" }
];

export const samplePeriodsData: Period[] = [
  {
    period: "1",
    grade: "4",
    subject: "Math",
    time: 30,
    open_default: 15,
    close_default: 15
  },
  {
    period: "2",
    grade: "3",
    subject: "Math",
    time: 30,
    open_default: 15,
    close_default: 15
  },
  {
    period: "3",
    grade: "4",
    subject: "Science",
    time: 30,
    open_default: 15,
    close_default: 15
  }
];

export const sampleStandardsData = standards;

export const sampleReportData: UserStandards[] = [
  {
    date: new Date(2022, 2, 9),
    standard: standards["1_ENGLISH"][0],
    time: 30,
    link: "/myplans/0",
    dok1: { lessons: [new Date(2022, 1, 20)], time: 30 },
    dok2: { lessons: [new Date(2022, 1, 20)], time: 30 },
    dok3: { lessons: [new Date(2022, 1, 20)], time: 30 },
    dok4: { lessons: [new Date(2022, 1, 20)], time: 30 }
  },
  {
    date: new Date(2022, 2, 8),
    standard: standards["4_MATH"][0],
    time: 30,
    link: "/myplans/0",
    dok1: { lessons: [], time: 30 },
    dok2: { lessons: [], time: 30 },
    dok3: { lessons: [], time: 30 },
    dok4: { lessons: [], time: 30 }
  }
];

export const sampleLessonData: Lesson[] = [
  {
    finished: true,
    period: [{ period: "1", grade: "4", subject: "Math", time: 30, open_default: 15, close_default: 15 }],
    planName: "Name 1",
    date: {
      start: new Date("2022-02-21T05:00:00.000Z"),
      end: new Date("2022-02-21T05:00:00.000Z")
    },
    subjects: [],
    careers: [],
    standardsChosen: [
      {
        band_course: null,
        content_area: "OP & ALG",
        grade: "4",
        standard:
          "Interpret a multiplication equation as a comparison, e.g., interpret 35 = 5 × 7 as a statement that 35 is 5 times as many as 7 and 7 times as many as 5. Represent verbal statements of multiplicative comparisons as multiplication equations.",
        standardId: "4.OA.1",
        subject: "MATH"
      }
    ],
    lessons: [
      {
        opening: {
          time: 15,
          strategy: ["Bell Ringer"],
          details: "details open"
        },
        body: [
          {
            time: 20,
            workType: "individual",
            workTypeDetails: "work type details",
            materials: "materials",
            strategy: ["Classroom Discussion"],
            standards: [
              {
                standard: {
                  band_course: null,
                  content_area: "OP & ALG",
                  grade: "4",
                  standard:
                    "Interpret a multiplication equation as a comparison, e.g., interpret 35 = 5 × 7 as a statement that 35 is 5 times as many as 7 and 7 times as many as 5. Represent verbal statements of multiplicative comparisons as multiplication equations.",
                  standardId: "4.OA.1",
                  subject: "MATH"
                },
                dok: "dok1"
              }
            ]
          }
        ],
        closing: {
          time: 25,
          strategy: ["Reading"],
          details: "details"
        },
        date: new Date("2022-02-21T05:00:00.000Z")
      },
      {
        opening: {
          time: 15,
          strategy: ["Bell Ringer"],
          details: "details open"
        },
        body: [
          {
            time: 20,
            workType: "individual",
            workTypeDetails: "work type details",
            materials: "materials",
            strategy: ["Classroom Discussion"],
            standards: [
              {
                standard: {
                  band_course: null,
                  content_area: "OP & ALG",
                  grade: "4",
                  standard:
                    "Interpret a multiplication equation as a comparison, e.g., interpret 35 = 5 × 7 as a statement that 35 is 5 times as many as 7 and 7 times as many as 5. Represent verbal statements of multiplicative comparisons as multiplication equations.",
                  standardId: "4.OA.1",
                  subject: "MATH"
                },
                dok: "dok1"
              }
            ]
          }
        ],
        closing: {
          time: 25,
          strategy: ["Reading"],
          details: "details"
        },
        date: new Date("2022-02-21T05:00:00.000Z")
      }
    ]
  }
];
