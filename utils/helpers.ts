import dayjs from "dayjs";
import { Grades } from "enums";
import { Period } from "interfaces";

export const formattedDate = (date: Date | string) => dayjs(date).format("MM/DD/YY");

export const getDatesBetweenDates = (startDate: Date | string, endDate: Date | string) => {
  const startDateZero = dayjs(startDate).startOf("day");
  const endDateZero = dayjs(endDate).startOf("day");
  let dates: Date[] = [];

  let theDate = dayjs(startDateZero);

  while (theDate <= endDateZero) {
    const newDate = dayjs(theDate);
    if (newDate.get("day") !== 6 && newDate.get("day") !== 0) {
      dates = [...dates, dayjs(theDate).toDate()];
    }

    theDate = newDate.add(1, "day");
  }
  return dates;
};

export const shortestTime = (period: Period[], openOrClose: "open" | "close") => {
  const shortestN = period.reduce((prev, curr) => {
    const currOpen = curr[`${openOrClose}_default`];
    const prevOpen = prev[`${openOrClose}_default`];

    return currOpen < prevOpen ? curr : prev;
  }, period[0]);

  return shortestN[`${openOrClose}_default`];
};

export const numberOrdinal = (n: Grades) => {
  if (n === Grades.KINDERGARTEN) return n;
  const nInt = parseInt(n, 10);
  const s = ["th", "st", "nd", "rd"];
  const v = nInt % 100;
  return `${nInt + (s[(v - 20) % 10] || s[v] || s[0])} grade`;
};

const getStandardsUsage = lessons => {
  let usage = {};

  lessons.forEach(lesson => {
    lesson.body.forEach(b => {
      const { time } = b;
      usage = reduceStandards(b.standards, time, "date", usage);
    });
  });

  return usage;
};

const reduceStandards = (standards, time, date, usage) => {
  standards.forEach(s => {
    const id = s.standard.standardId;
    const dokLevel = s.dok;

    if (id in usage) {
      if (dokLevel in usage[id]) {
        const prev = usage[id];

        usage[id] = {
          ...usage[id],
          [dokLevel]: {
            ...prev[dokLevel],
            time: prev.time + time,
            lastUsed: "LESSON ID HERE",
            timesUsed: prev.timesUsed + 1,
            lastDate: date
          }
        };
      } else {
        usage[id] = {
          ...usage[id],
          [dokLevel]: {
            lastDate: date,
            lastUsed: "LESSON ID HERE",
            time: time,
            timesUsed: 1
          }
        };
      }
    } else {
      usage[id] = {
        [dokLevel]: {
          lastDate: date,
          lastUsed: "LESSON ID HERE",
          time: time,
          timesUsed: 1
        }
      };
    }
  });

  return usage;
};
