import { useQuery } from "react-query";

import firebaseLessonService from "../services/firebaseLessonService";

const fetcher = async (date: Date) => {
  const data = await firebaseLessonService.findLessonsByDate(date);
  console.log(data);
  return data;
};

const usePlansByDate = date => {
  const myQuery = useQuery(["lessonPlansDate", date], () => fetcher(date), {
    refetchInterval: false,
    refetchIntervalInBackground: false,
    refetchOnMount: false,
    refetchOnWindowFocus: false,
    refetchOnReconnect: false
  });

  return myQuery;
};

export default usePlansByDate;
