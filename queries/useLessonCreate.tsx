import { useMutation } from "react-query";
import firebaseLessonService from "services/firebaseLessonService";

const saveDate = data => firebaseLessonService.createLesson(data);

const useLessonCreate = () => {
  const mutation = useMutation(data => saveDate(data));

  const submit = async data => {
    mutation.mutate(data);
  };

  return { submit, isLoading: mutation.isLoading };
};

export default useLessonCreate;
