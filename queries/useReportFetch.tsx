import { useQuery } from "react-query";
// import firebaseLessonService from "services/firebaseLessonService";
import firebaseStandardsUsageService from "services/firebaseStandardsUsageService";

const fetcher = async (date: Date) => {
  // const results = await firebaseLessonService.findLessonsForReport(date);
  const results = await firebaseStandardsUsageService.findByUser();
  // const results = await fetch(`/api/report/${date.toISOString()}`).then(res => res.json());
  return results;
};

const useReportFetch = date => {
  const myQuery = useQuery(["report", date], () => fetcher(date), { enabled: !!date });

  return myQuery;
};

export default useReportFetch;
