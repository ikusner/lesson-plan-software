import { useMutation } from "react-query";
import firebasePeriodsService from "services/firebasePeriodsService";

const saveDate = data => firebasePeriodsService.create(data);

const usePeriodsUpsert = () => {
  const mutation = useMutation(data => saveDate(data));

  const submit = async data => {
    mutation.mutate(data);
  };

  return { submit };
};

export default usePeriodsUpsert;
