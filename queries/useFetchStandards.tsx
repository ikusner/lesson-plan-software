import { useQuery } from "react-query";

const fetcher = periods => {
  const parsed = periods.map(p => `${p.grade}_${p.subject.toUpperCase()}`);

  return fetch("/api/standards?find=K_MATH", {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(parsed)
  }).then(res => res.json());
};

const useFetchStandards = periods => {
  const myQuery = useQuery(["standards"], () => fetcher(periods), {
    enabled: false
  });

  return myQuery;
};

export default useFetchStandards;
