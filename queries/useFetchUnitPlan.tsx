import { useQuery } from "react-query";
import firebaseLessonService from "services/firebaseLessonService";

const fetcher = id => firebaseLessonService.findById(id);
const fetcher2 = id => fetch(`/api/lessonPlans/${0}`).then(res => res.json());

const useFetchUnitPlan = id => {
  const myQuery = useQuery(["lessonPlan", id], () => fetcher2(id), {
    refetchInterval: false,
    refetchIntervalInBackground: false,
    enabled: id?.length > 0
  });

  return myQuery;
};

export default useFetchUnitPlan;
