import { useQuery } from "react-query";
import firebaseLessonService from "services/firebaseLessonService";

const fetcher = async () => firebaseLessonService.findLessonsToRate();

const useFetchToRate = () => {
  const myQuery = useQuery(["lessonPlansToRate"], fetcher, {
    refetchInterval: false,
    refetchIntervalInBackground: false,
    refetchOnMount: false,
    refetchOnWindowFocus: false,
    refetchOnReconnect: false
  });

  return myQuery;
};

export default useFetchToRate;
