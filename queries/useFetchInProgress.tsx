import { useQuery } from "react-query";
import firebaseLessonService from "services/firebaseLessonService";

const fetcher = () => firebaseLessonService.findLessonsInProgress();

const useFetchInProgress = () => {
  const myQuery = useQuery(["lessonPlansInProgress"], fetcher, {
    refetchInterval: false,
    refetchIntervalInBackground: false,
    refetchOnMount: false,
    refetchOnWindowFocus: false,
    refetchOnReconnect: false
  });

  return myQuery;
};

export default useFetchInProgress;
