import { useQuery } from "react-query";

import firebasePeriodsService from "../services/firebasePeriodsService";

const fetcher = async () => {
  const data = await firebasePeriodsService.findAll();
  return data;
};

const userPeriodsFetch = () => {
  const myQuery = useQuery("userPeriods", fetcher);

  return myQuery;
};

export default userPeriodsFetch;
