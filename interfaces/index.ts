export interface User {
  id: number;
  name: string;
  subjects: string[];
  grade: string;
}

export interface Period {
  period: string;
  grade: string;
  subject: string;
  time: number;
  open_default: number;
  close_default: number;
}

interface Standard {
  standardId: string;
  grade: string;
  subject: string;
  band_course: string | null;
  content_area: string;
  standard: string;
}

export interface StandardsJSON {
  K_ENGLISH: Standard[];
  "1_ENGLISH": Standard[];
  "2_ENGLISH": Standard[];
  "3_ENGLISH": Standard[];
  "4_ENGLISH": Standard[];
  "5_ENGLISH": Standard[];
  "6_ENGLISH": Standard[];
  "7_ENGLISH": Standard[];
  "8_ENGLISH": Standard[];
  HS_ENGLISH: Standard[];
  ANCHOR_ENGLISH: Standard[];
  K_HEALTH: Standard[];
  "1_HEALTH": Standard[];
  "2_HEALTH": Standard[];
  "3_HEALTH": Standard[];
  "4_HEALTH": Standard[];
  "5_HEALTH": Standard[];
  "6_HEALTH": Standard[];
  "7_HEALTH": Standard[];
  "8_HEALTH": Standard[];
  HS_HEALTH: Standard[];
  K_MATH: Standard[];
  "1_MATH": Standard[];
  "2_MATH": Standard[];
  "3_MATH": Standard[];
  "4_MATH": Standard[];
  "5_MATH": Standard[];
  "6_MATH": Standard[];
  "7_MATH": Standard[];
  "8_MATH": Standard[];
  HS_MATH: Standard[];
  "K_PHYS ED": Standard[];
  "1_PHYS ED": Standard[];
  "2_PHYS ED": Standard[];
  "3_PHYS ED": Standard[];
  "4_PHYS ED": Standard[];
  "5_PHYS ED": Standard[];
  "6_PHYS ED": Standard[];
  "7_PHYS ED": Standard[];
  "8_PHYS ED": Standard[];
  "HS_PHYS ED": Standard[];
  K_SCIENCE: Standard[];
  "1_SCIENCE": Standard[];
  "2_SCIENCE": Standard[];
  "3_SCIENCE": Standard[];
  "4_SCIENCE": Standard[];
  "5_SCIENCE": Standard[];
  "6_SCIENCE": Standard[];
  "7_SCIENCE": Standard[];
  "8_SCIENCE": Standard[];
  HS_SCIENCE: Standard[];
  "K_SOC ST": Standard[];
  "1_SOC ST": Standard[];
  "2_SOC ST": Standard[];
  "3_SOC ST": Standard[];
  "4_SOC ST": Standard[];
  "5_SOC ST": Standard[];
  "6_SOC ST": Standard[];
  "7_SOC ST": Standard[];
  "8_SOC ST": Standard[];
  "HS_SOC ST": Standard[];
}

export interface UserStandards {
  date: Date;
  standard: Standard;
  time: number;
  link: string;
  dok1: { lessons: Date[]; time: number };
  dok2: { lessons: Date[]; time: number };
  dok3: { lessons: Date[]; time: number };
  dok4: { lessons: Date[]; time: number };
}

interface LessonOpenClose {
  time: number;
  strategy: string[];
  details: string;
}

interface LessonBody {
  time: number;
  workType: string;
  workTypeDetails: string;
  materials: string;
  strategy: string[];
  standards: {
    standard: Standard;
    dok: string;
  }[];
}

interface LessonContent {
  opening: LessonOpenClose;
  closing: LessonOpenClose;
  body: LessonBody[];
  date: Date;
}

export interface Lesson {
  period: Period[];
  planName: string;
  date: { start: Date; end: Date };
  subjects: string[];
  careers: string[];
  standardsChosen: Standard[];
  lessons: LessonContent[];
  finished: boolean;
}
