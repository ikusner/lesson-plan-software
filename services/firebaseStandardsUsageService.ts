import { query, where } from "firebase/firestore";

import FirebaseService from "./firebaseService";

class FirebaseStandardsUsageService extends FirebaseService {
  public findByUser = async () => {
    try {
      const tempUser = "change to user ref";

      const q = query(this.collectionRef, where("userId", "==", tempUser));

      const standards = await this.findByQuery(q);

      // const test = await getDoc(standards[0].dok1.lastUsed);
      // if (test.exists()) {
      //   console.log(test.data());
      // }

      // eslint-disable-next-line arrow-body-style
      const results = standards.map(_doc => {
        return {
          ..._doc,
          dok1: { ..._doc.dok1, lastUsed: _doc.dok1.lastUsed.id },
          dok2: { ..._doc.dok2, lastUsed: _doc.dok2.lastUsed.id },
          dok3: { ..._doc.dok3, lastUsed: _doc.dok3.lastUsed.id },
          dok4: { ..._doc.dok4, lastUsed: _doc.dok4.lastUsed.id }
        };
      });

      return results;
    } catch (err) {
      return console.log(err);
    }
  };
}

const firebaseStandardsUsageService = new FirebaseStandardsUsageService("standardsUsage");

export default firebaseStandardsUsageService;
