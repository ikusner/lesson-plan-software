import {
  addDoc,
  collection,
  CollectionReference,
  deleteDoc,
  doc,
  getDoc,
  getDocs,
  Query,
  updateDoc
} from "firebase/firestore";

import { db } from "../firebase";

export default class FirebaseService {
  collectionRef: CollectionReference;

  constructor(collectionName: string) {
    this.collectionRef = collection(db, collectionName);
  }

  // get one document from collection
  public findById = async (id: string) => {
    const documentRef = doc(this.collectionRef, id);
    const snapshot = await getDoc(documentRef);

    if (snapshot.exists()) {
      return snapshot.data();
    }

    return null;
  };

  // get multiple documents from collection
  public findAll = async () => {
    const snapshot = await getDocs(this.collectionRef);

    const data = snapshot.docs.map(_doc => _doc.data());

    console.log("DATA", data);

    return data;
  };

  protected findByQuery = async (query: Query) => {
    const querySnapshot = await getDocs(query);

    const data = querySnapshot.docs.map(_doc => _doc.data());

    return data;
  };

  // create a document
  public create = async data => {
    await addDoc(this.collectionRef, data);
  };

  // create
  public update = async (id: string, data) => {
    const documentRef = doc(this.collectionRef, id);

    return updateDoc(documentRef, data);
  };

  public remove = async (id: string) => {
    const documentRef = doc(this.collectionRef, id);

    return deleteDoc(documentRef);
  };

  protected useJSDates = data => {
    console.log("length", data.length);
    const withJSDates = data.map(_d => {
      // eslint-disable-next-line no-param-reassign
      console.log("data", _d.date);
      _d.date = { start: _d.date.start.toDate(), end: _d.date.end.toDate() };
      console.log("_d", _d);
      return _d;
    });

    return withJSDates;
  };
}
