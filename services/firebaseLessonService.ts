import { query, where } from "firebase/firestore";

import FirebaseService from "./firebaseService";

class FirebaseLessonService extends FirebaseService {
  public findAllLessons = async () => {
    const data = await this.findAll();

    return this.useJSDates(data);
  };

  public findLessonsByDate = async (date: Date) => {
    const q = query(this.collectionRef, where("date.end", ">=", date));

    const docsFound = await this.findByQuery(q);

    const filtered = docsFound.filter(_doc => _doc.date.start.toDate() <= date);

    return this.useJSDates(filtered);
  };

  public findLessonsInProgress = async () => {
    const q = query(this.collectionRef, where("finished", "==", false));

    const docsFound = await this.findByQuery(q);

    console.log(docsFound);

    return this.useJSDates(docsFound);
  };

  public findLessonsToRate = async () => {
    const q = query(this.collectionRef, where("rating", "==", 0));

    const docsFound = await this.findByQuery(q);

    return this.useJSDates(docsFound);
  };

  public findLessonsForReport = async (date: Date) => {
    const today = new Date();
    const currentYear = today.getMonth() > 7 ? today.getFullYear() : today.getFullYear() - 1;

    const beginningOfSchoolYear = new Date(currentYear, 7, 1);

    const q = query(this.collectionRef, where("date.end", "<=", date));

    const docsFound = await this.findByQuery(q);

    const filtered = docsFound.filter(_doc => _doc.date.start.toDate() >= beginningOfSchoolYear);

    return this.useJSDates(filtered);
  };

  public createLesson = async data => {
    const dataCopy = data;

    dataCopy.date.start = new Date(dataCopy.date.start);
    dataCopy.date.end = new Date(dataCopy.date.end);

    return this.create(dataCopy);
  };
}

const firebaseLessonService = new FirebaseLessonService("lessons");

export default firebaseLessonService;
