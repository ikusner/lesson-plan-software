import FirebaseService from "./firebaseService";

const firebasePeriodsService = new FirebaseService("periods");

export default firebasePeriodsService;
