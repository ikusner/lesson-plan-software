import { Col, Grid } from "@mantine/core";
import usePlansByDate from "queries/usePlansByDate";
import React, { useState } from "react";

import LessonCalendar from "../components/LessonCalendar";
import UnitPlanCard from "../components/UnitPlanCard";

const LessonView = () => {
  const [date, setDate] = useState(new Date(new Date().setHours(0, 0, 0, 0)));
  const query = usePlansByDate(date);

  return (
    <>
      <LessonCalendar callback={setDate} value={date} />
      {query.isSuccess && (
        <Grid>
          {query.data.map((lesson, i) => (
            <Col span={12} key={i}>
              <UnitPlanCard name={lesson.planName} grade={lesson.period[0].grade} date={lesson.date} />
            </Col>
          ))}
        </Grid>
      )}
    </>
  );
};

export default LessonView;
