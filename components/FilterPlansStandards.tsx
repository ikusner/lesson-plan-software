import { Button, Center, Grid, Text } from "@mantine/core";
import { Grades, Subjects } from "enums";
import React, { useEffect, useState } from "react";
import { NestedValue, useForm, useWatch } from "react-hook-form";

import { MultiSelect, Select } from "./formInputs";

interface planFilterProps {
  date: NestedValue<{
    start: Date | string;
    end: Date | string;
  }>;
  title: Array<string>;
  rating: Array<string>;
  favorites: Array<string>;
  subjects: Array<string>;
}

const dropdownTest = [
  {
    value: "1",
    label: "1"
  },
  {
    value: "2",
    label: "2"
  }
];

const FilterPlansStandards = () => {
  const [contentArea, setContentArea] = useState([dropdownTest[1]]);
  const [standards, setStandards] = useState([dropdownTest[1]]);

  const onSubmit = async formData => {
    console.log(formData);
  };

  const form = useForm({
    defaultValues: {
      grade: "",
      subjects: [],
      contentArea: "",
      standards: []
    }
  });
  const { control, handleSubmit, setValue } = form;

  const standardFields = useWatch({
    control,
    name: ["grade", "subjects", "contentArea"]
  });

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Grid my={15} justify="center">
        <Grid.Col span={2} style={{ display: "flex", alignItems: "flex-end", paddingBottom: 12 }}>
          <Text>Search by Standard:</Text>
        </Grid.Col>
        <Grid.Col span={2}>
          <Select
            data={Object.keys(Grades).map(e => ({
              value: Grades[e],
              label: Grades[e]
            }))}
            name="grade"
            control={control}
            placeholder="Grade(s)"
            label="Grade(s)"
            onChange={e => {
              setValue("grade", e);

              // set other fields
              setValue("subjects", []);
              setValue("contentArea", "");
              setValue("standards", []);
            }}
          />
        </Grid.Col>
        <Grid.Col span={2}>
          {standardFields[0] && (
            <MultiSelect
              data={Object.keys(Subjects).map(e => ({
                value: Subjects[e],
                label: Subjects[e]
              }))}
              name="subjects"
              control={control}
              placeholder="Subject(s)"
              label="Subject(s)"
              onChange={e => {
                console.log(e);
                setValue("subjects", e);
                if (e?.length === 0) {
                  setValue("contentArea", "");
                  setValue("standards", []);
                }

                if (e.includes(Subjects.MATH)) {
                  console.log("contains math");
                  setContentArea([dropdownTest[0]]);
                }
              }}
            />
          )}
        </Grid.Col>
        <Grid.Col span={2}>
          {standardFields[1]?.length > 0 && (
            <Select
              data={contentArea}
              name="contentArea"
              control={control}
              placeholder="Content Area"
              label="Content Area"
              onChange={e => {
                setValue("contentArea", e);
                setValue("standards", []);
              }}
            />
          )}
        </Grid.Col>
        <Grid.Col span={2}>
          {standardFields[1]?.length > 0 && standardFields[2] && (
            <MultiSelect
              data={dropdownTest}
              name="standards"
              control={control}
              placeholder="Standard(s)"
              label="Standard(s)"
            />
          )}
        </Grid.Col>
      </Grid>
      <Center my={15}>
        <Button type="submit">Search</Button>
      </Center>
    </form>
  );
};

export default FilterPlansStandards;
