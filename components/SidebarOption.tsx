import { Box } from "@mantine/core";
import Link from "next/link";
import React from "react";

interface Props {
  link: string;
  children: React.ReactNode;
  callback?: () => void;
}

const SidebarOption = ({ children, link, callback = () => {} }: Props) => (
  <Link href={link}>
    <Box
      onClick={callback}
      component="a"
      sx={theme => ({
        // width: "100%",
        display: "block",
        userSelect: "none",
        textAlign: "left",
        padding: theme.spacing.sm,
        borderRadius: theme.radius.md,
        cursor: "pointer",

        "&:hover": {
          backgroundColor: theme.colorScheme === "dark" ? theme.colors.dark[5] : theme.colors.gray[0]
        }
      })}
    >
      {children}
    </Box>
  </Link>
);

export default SidebarOption;
