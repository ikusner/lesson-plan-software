import { Table } from "@mantine/core";
import { useTable } from "react-table";

// eslint-disable-next-line no-unused-vars
const StyledTable = ({ columns, data, ...rest }) => {
  const { getTableProps, getTableBodyProps, headerGroups, rows, prepareRow } = useTable({
    columns,
    data
  });
  return (
    <Table
      sx={theme => ({
        border: "solid black 1px",

        "tbody tr:nth-of-type(odd)": {
          backgroundColor: theme.colors.brand[6],
          color: "white"
        },

        // "thead > tr:nth-of-type(2) > th:nth-child(n+2)": {
        //   textAlign: "center",
        //   color: "red"
        // },

        "tbody > tr > td": {
          borderBottom: "solid black 1px"
        },

        // "tbody > tr > td:nth-child(n+2)": {
        //   textAlign: "center",
        //   color: "green"
        // },
        // "thead > tr:nth-of-type(1) > th": {
        //   textAlign: "center"
        // },

        "thead > tr > th": {
          borderBottom: "none"
        },
        "tbody > tr > td:first-child": {
          maxWidth: "300px"
        }
      })}
      {...getTableProps()}
    >
      <thead>
        {headerGroups.map(headerGroup => (
          <tr {...headerGroup.getHeaderGroupProps()}>
            {headerGroup.headers.map(column => (
              <th {...column.getHeaderProps()}>{column.render("Header")}</th>
            ))}
          </tr>
        ))}
      </thead>
      <tbody {...getTableBodyProps()}>
        {rows.map(row => {
          prepareRow(row);
          return (
            <tr {...row.getRowProps()}>
              {row.cells.map(cell => (
                <td {...cell.getCellProps()}>{cell.render("Cell")}</td>
              ))}
            </tr>
          );
        })}
      </tbody>
    </Table>
  );
};

export default StyledTable;
