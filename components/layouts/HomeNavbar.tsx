import { AppShell, Burger, Header, MediaQuery, Navbar, Text } from "@mantine/core";
import Image from "next/image";
import { useState } from "react";

import NavbarMenu from "../NavbarMenu";

const HomeNavbar = ({ children }) => {
  const [opened, setOpened] = useState(false);

  return (
    <AppShell
      // navbarOffsetBreakpoint controls when navbar should no longer be offset with padding-left
      navbarOffsetBreakpoint="sm"
      // fixed prop on AppShell will be automatically added to Header and Navbar
      fixed
      navbar={
        <Navbar
          padding="md"
          // Breakpoint at which navbar will be hidden if hidden prop is true
          hiddenBreakpoint="lg"
          // Hides navbar when viewport size is less than value specified in hiddenBreakpoint
          hidden={!opened}
          width={{ lg: 200 }}
        >
          <NavbarMenu setClosed={() => setOpened(o => !o)} />
        </Navbar>
      }
      header={
        <Header height={105} padding="md">
          {/* You can handle other responsive styles with MediaQuery component or createStyles function */}
          <div
            style={{
              display: "flex",
              alignItems: "center",
              height: "100%"
            }}
          >
            <MediaQuery largerThan="lg" styles={{ display: "none" }}>
              <Burger
                style={{ position: "relative", left: 0 }}
                opened={opened}
                onClick={() => setOpened(o => !o)}
                size="md"
                mr="xl"
              />
            </MediaQuery>
            <div
              style={{
                position: "relative",
                width: "100px",
                height: "100px"
              }}
            >
              <MediaQuery smallerThan="lg" styles={{ display: "none !important" }}>
                <Image src="/images/scc-logo.jpg" alt="logo" layout="fill" />
              </MediaQuery>
            </div>
            <MediaQuery largerThan="lg" styles={{ display: "none !important" }}>
              <div style={{ width: "100%", display: "flex", alignItems: "center", justifyContent: "center" }}>
                <div
                  style={{
                    position: "relative",
                    width: "100px",
                    height: "100px"
                  }}
                >
                  <Image src="/images/scc-logo.jpg" alt="logo" layout="fill" />
                </div>
              </div>
            </MediaQuery>
            <Text style={{ width: "175px", margin: "auto 0 auto auto", textAlign: "center" }}>Hello, Name</Text>
          </div>
        </Header>
      }
    >
      {children}
    </AppShell>
  );
};

export default HomeNavbar;
