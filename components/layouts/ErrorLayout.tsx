import Head from "next/head";

import HomeNavbar from "./HomeNavbar";

const ErrorLayout = ({ children, head }) => (
  <>
    <Head>
      <title>{head}</title>
      <meta name="viewport" content="minimum-scale=1, initial-scale=1, width=device-width" />
    </Head>
    <HomeNavbar>{children}</HomeNavbar>;
  </>
);

export default ErrorLayout;
