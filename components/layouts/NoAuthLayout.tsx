import Head from "next/head";

const NoAuthLayout = ({ children, head }) => (
  <>
    <Head>
      <title>{head}</title>
      <meta name="viewport" content="minimum-scale=1, initial-scale=1, width=device-width" />
    </Head>
    {children}
  </>
);

export default NoAuthLayout;
