import type { SelectProps } from "@mantine/core";
import { Select as Mantine } from "@mantine/core";
import { Controller } from "react-hook-form";

interface Props extends SelectProps {
  control: any;
  name: string;
}

const Select = ({ name, control, ...rest }: Props) => (
  <Controller name={name} control={control} render={({ field }) => <Mantine {...field} {...rest} />} />
);

export default Select;
