import { RadioGroup as Mantine, RadioGroupProps } from "@mantine/core";
import React from "react";
import { Control, Controller } from "react-hook-form";

interface Props extends RadioGroupProps {
  control: Control;
  name: string;
  children: React.ReactNode;
}

const RadioGroup = ({ control, name, children, ...rest }: Props) => (
  <div style={{ margin: "20px" }}>
    <Controller
      name={name}
      control={control}
      render={({ field }) => (
        <Mantine variant="vertical" {...field} {...rest}>
          {children}
        </Mantine>
      )}
    />
  </div>
);

export default RadioGroup;
