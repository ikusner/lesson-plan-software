import { Textarea as Mantine, TextareaProps } from "@mantine/core";
import { Control, Controller } from "react-hook-form";

interface Props extends TextareaProps {
  control: Control;
  name: string;
}

const Textarea = ({ control, name, ...rest }: Props) => (
  <Controller
    name={name}
    control={control}
    render={({ field }) => <Mantine autosize minRows={2} maxRows={4} {...field} {...rest} />}
  />
);

export default Textarea;
