import { Chips as Mantine, InputWrapper } from "@mantine/core";
import React from "react";
import { Control, Controller } from "react-hook-form";

interface Props {
  id: string;
  name: string;
  control: Control;
  label: string;
  children: React.ReactNode;
}

const Chips = ({ name, control, id, label, children, ...rest }: Props) => (
  <Controller
    name={name}
    control={control}
    render={({ field: { ref, ...fieldValues } }) => (
      <InputWrapper id={id} label={label}>
        <Mantine id={id} {...fieldValues} {...rest}>
          {children}
        </Mantine>
      </InputWrapper>
    )}
  />
);

export default Chips;
