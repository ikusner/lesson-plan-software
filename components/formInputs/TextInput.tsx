import { TextInput as Mantine } from "@mantine/core";
import { Control, Controller } from "react-hook-form";

interface Props {
  label: string;
  placeholder: string;
  control: Control;
  name: string;
}

const TextInput = ({ control, placeholder, label, name }: Props) => (
  <Controller
    name={name}
    control={control}
    render={({ field, fieldState: { error } }) => (
      <Mantine error={error?.message} placeholder={placeholder} label={label} {...field} />
    )}
  />
);

export default TextInput;
