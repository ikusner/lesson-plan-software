import { Checkbox, Chip, Chips, Group, InputWrapper, Tooltip } from "@mantine/core";
import { QuestionMarkCircledIcon } from "@radix-ui/react-icons";
import { Control, Controller, useFormContext } from "react-hook-form";

interface Props {
  control: Control;
  name: string;
  label: string;
}

const CheckboxChip = ({ control, name, label }: Props) => {
  const { getValues, watch, setValue } = useFormContext();
  const selectedStandards = getValues("standardsChosen");
  const checkboxes = watch(name);

  const handleCheck = item => {
    const ids = getValues(name);
    const newIds = ids?.some(el => el.standard.standardId === item.standardId);

    if (newIds) {
      return ids?.filter(el => el.standard.standardId !== item.standardId);
    }

    return [...(ids ?? []), { standard: item, dok: "" }];
  };

  const handleRadio = (item, dok) => {
    const ids = getValues(name);
    const foundIndex = ids ? ids?.findIndex(x => x.standard.standardId === item.standardId) : -1;
    const newIds = ids;

    if (foundIndex !== -1) {
      newIds[foundIndex] = { standard: item, dok };
    }

    return newIds;
  };

  return (
    <Controller
      name={name}
      control={control}
      render={({ field: { ref, ...fieldValues } }) => (
        <div>
          <InputWrapper id={name} label={label}>
            {selectedStandards.map((item, index) => (
              <div key={item.standardId} id={name}>
                <Checkbox
                  {...fieldValues}
                  ref={ref}
                  style={{ marginTop: 15 }}
                  label={item.standard}
                  checked={getValues(name)?.findIndex(el => el?.standard.standardId === item.standardId) !== -1}
                  onChange={() => {
                    const newItem = handleCheck(item);
                    setValue(name, newItem);
                  }}
                  key={index}
                />
                <Group id={name}>
                  {checkboxes?.some(el => el.standard.standardId === item.standardId) && (
                    <>
                      <Chips
                        {...fieldValues}
                        value={checkboxes.find(el => el.standard.standardId === item.standardId)?.dok}
                        onChange={value => {
                          setValue(name, handleRadio(item, value));
                        }}
                        size="xs"
                      >
                        <Tooltip opened label="dok1" position="top" placement="center">
                          <Chip value="dok1">DOK1</Chip>
                        </Tooltip>
                        <Chip value="dok1">DOK1</Chip>
                        <Chip value="dok2">DOK2</Chip>
                        <Chip value="dok3">DOK3</Chip>
                        <Chip value="dok4">DOK4</Chip>
                      </Chips>
                      <Tooltip
                        wrapLines
                        width={200}
                        label="Dok1: Recall / reproduce Dok2: Skills and concepts Dok3: Strategic thinking Dok4: Extended thinking"
                        position="right"
                        placement="center"
                        withArrow
                        style={{ marginLeft: "5px" }}
                      >
                        <QuestionMarkCircledIcon />
                      </Tooltip>
                    </>
                  )}
                </Group>
              </div>
            ))}
          </InputWrapper>
        </div>
      )}
    />
  );
};

export default CheckboxChip;
