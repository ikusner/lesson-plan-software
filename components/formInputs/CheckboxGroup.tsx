import { Checkbox } from "@mantine/core";
import { Control, Controller, useFormContext } from "react-hook-form";

interface Props {
  control: Control;
  name: string;
  items: Array<{
    id: string | number;
    label: string;
  }>;
}

const CheckboxGroup = ({ control, name, items, ...rest }: Props) => {
  const { getValues, setValue } = useFormContext();

  const handleCheck = item => {
    const { [name]: ids } = getValues();
    const newIds = ids?.some(el => el.standardId === item.id);

    if (newIds) {
      return ids?.filter(el => el.standardId !== item.id);
    }

    return [...(ids ?? []), item.value];
  };

  return (
    <div style={{ margin: "20px" }}>
      <Controller
        name={name}
        control={control}
        render={({ field }) => (
          <>
            {items.map((item, index) => (
              <Checkbox
                style={{ margin: "10px" }}
                key={index}
                {...field}
                {...rest}
                label={item.label}
                checked={getValues(name)?.findIndex(el => el.standardId === item.id) !== -1}
                onChange={() => {
                  setValue(name, handleCheck(item));
                }}
              />
            ))}
          </>
        )}
      />
    </div>
  );
};

export default CheckboxGroup;
