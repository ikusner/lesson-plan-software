import type { MultiSelectProps } from "@mantine/core";
import { MultiSelect as Mantine } from "@mantine/core";
import { Controller } from "react-hook-form";

interface Props extends MultiSelectProps {
  control: any;
  name: string;
}

const MultiSelect = ({ name, control, ...rest }: Props) => (
  <Controller
    name={name}
    control={control}
    defaultValue={[]}
    render={({ field, fieldState: { error } }) => <Mantine error={error?.message} {...field} {...rest} />}
  />
);

export default MultiSelect;
