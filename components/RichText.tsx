// RichText.tsx in your components folder
import { InputWrapper } from "@mantine/core";
import dynamic from "next/dynamic";
import { Control, Controller } from "react-hook-form";

const RichText = dynamic(() => import("@mantine/rte"), {
  // Disable during server side rendering
  ssr: false,

  // Render anything as fallback on server, e.g. loader or html content without editor
  loading: () => null
});

interface Props {
  control: Control;
  label: string;
  name: string;
}

const controls: any[][] = [
  ["bold", "italic", "underline", "strike"],
  ["unorderedList", "orderedList", "h1", "h2", "h3"],
  ["link", "video"],
  ["alignLeft", "alignCenter", "alignRight"],
  ["sup", "sub"],
  ["clean"]
];

const RichTextEditor = ({ label, control, name, ...rest }: Props) => (
  <Controller
    name={name}
    control={control}
    render={({ field }) => (
      <InputWrapper label={label}>
        <RichText controls={controls} {...field} {...rest} />
      </InputWrapper>
    )}
  />
);

export default RichTextEditor;
