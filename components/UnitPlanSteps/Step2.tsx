import { Tabs } from "@mantine/core";
import dayjs from "dayjs";
import React from "react";
import { useFormContext } from "react-hook-form";

import LessonDay from "../LessonDay";

const Step2 = () => {
  const { control, getValues } = useFormContext();
  const lessons = getValues("lessons") || [];

  return (
    <div>
      <Tabs variant="pills" tabPadding="xl" position="center">
        {lessons.map((lesson, index) => (
          <Tabs.Tab label={dayjs(lesson.date)?.format("MM/DD/YYYY") || `date${index}`} key={index}>
            <LessonDay control={control} index={index} />
          </Tabs.Tab>
        ))}
      </Tabs>
    </div>
  );
};

export default Step2;
