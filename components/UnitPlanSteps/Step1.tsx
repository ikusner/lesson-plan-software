import PeriodSelect from "@components/PeriodSelect";
import { Col, Grid, Space, Text } from "@mantine/core";
import dayjs from "dayjs";
import { Subjects } from "enums";
import useFetchStandards from "queries/useFetchStandards";
import userPeriodsFetch from "queries/usePeriodsFetch";
import React, { useEffect, useState } from "react";
import { useFormContext } from "react-hook-form";
import { numberOrdinal } from "utils/helpers";

import careers from "../../constants/careers";
import { makePeriodDropdown, PeriodDropdown } from "../../fakeData/periods";
import { DatePicker, MultiSelect, TextInput } from "../formInputs";
import StandardSelection from "../StandardSelection";

const Step1 = () => {
  const {
    control,
    watch,
    setValue,
    getValues,
    formState: { errors }
  } = useFormContext();

  const startDate = watch("date.start");
  const periods = watch("period");
  const standardsChosen = watch("standardsChosen");
  const additionalSubjects = watch("subjects");

  const periodsQuery = userPeriodsFetch();
  const query = useFetchStandards(periods);

  const [myPeriods, setMyPeriods] = useState([]);

  useEffect(() => {
    const filteredStandards = standardsChosen.filter(standard => {
      const found = additionalSubjects.find(subject => subject.toUpperCase() === standard.subject.toUpperCase());

      if (found) {
        return true;
      }

      return false;
    });
    setValue("standardsChosen", filteredStandards);
  }, [additionalSubjects]);

  useEffect(() => {
    const filteredStandards = standardsChosen.filter(standard => {
      const found = periods.find(
        p => p.grade === standard.grade && p.subject.toUpperCase() === standard.subject.toUpperCase()
      );

      if (found) {
        return true;
      }

      return false;
    });
    setValue("standardsChosen", filteredStandards);

    if (periods.length === 1) {
      setMyPeriods(myPeriods.filter(p => p.grade === periods[0].grade));
    }

    if (periods.length === 0 && periodsQuery.data) {
      setMyPeriods(makePeriodDropdown(periodsQuery.data));
    }

    query.refetch().catch((err: any) => console.log(err));
  }, [periods, periodsQuery.data]);

  useEffect(() => {
    if (!periodsQuery.data) {
      return;
    }

    setMyPeriods(makePeriodDropdown(periodsQuery.data));
  }, [periodsQuery.data]);

  console.log("myPeriods", myPeriods);

  return (
    <div>
      <Text color="red">{errors?.standardsChosen && "Must choose at least one standard"}</Text>
      <Grid>
        <Col span={4}>
          <PeriodSelect all={periodsQuery.data} control={control} periodOptions={myPeriods} />
        </Col>
        <Col span={4}>
          <TextInput placeholder="My unit plan ..." label="Unit Plan Name" control={control} name="planName" />
        </Col>
        <Col span={2}>
          <DatePicker
            control={control}
            name="date.start"
            label="Start Date"
            placeholder={dayjs().format("MM-DD-YYYY")}
            minDate={dayjs(new Date()).startOf("d").toDate()}
            inputFormat="MM/DD/YYYY"
            labelFormat="MM/YYYY"
          />
        </Col>
        <Col span={2}>
          <DatePicker
            control={control}
            name="date.end"
            label="End Date"
            placeholder={dayjs().format("MM-DD-YYYY")}
            minDate={startDate}
            inputFormat="MM/DD/YYYY"
            labelFormat="MM/YYYY"
          />
        </Col>
      </Grid>
      <Space h="xl" />
      {periods.length > 0 && (
        <Text>
          You are planning for{" "}
          {periods.map((p, index) => {
            const last = periods.length > 1 && index < periods.length - 1;
            return (
              <span key={p.period}>
                {`${numberOrdinal(p.grade)} ${p.subject}`}
                {last ? " & " : null}
              </span>
            );
          })}
          {getValues("subjects")?.length > 0 && ` with ${getValues("subjects")?.join(",")}`}
        </Text>
      )}
      <Space h="xl" />
      <Grid>
        <Col span={6}>
          {periods.length > 0 && (
            <MultiSelect
              data={Object.keys(Subjects)
                .filter(subject => subject.toLowerCase() !== periods[0].subject.toLowerCase())
                .map(e => ({
                  value: Subjects[e],
                  label: Subjects[e]
                }))}
              name="subjects"
              placeholder="Additional Subjects to Integrate"
              control={control}
            />
          )}
        </Col>
      </Grid>
      <Space h="xl" />
      <StandardSelection />
      <Space h="xl" />
      <Grid>
        <Col span={4}>
          <MultiSelect
            data={careers}
            name="careers"
            label="Career Pathways"
            placeholder="Careers ..."
            control={control}
          />
        </Col>
      </Grid>
      <pre style={{ overflow: "auto" }}>{JSON.stringify(watch(), null, 2)}</pre>
    </div>
  );
};

export default Step1;
