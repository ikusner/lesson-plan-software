import { Divider, Group, Text, Title, UnstyledButton } from "@mantine/core";
import { PlusIcon } from "@radix-ui/react-icons";
import userPeriodsFetch from "queries/usePeriodsFetch";
import { useFormContext } from "react-hook-form";
import { formattedDate } from "utils/helpers";

import { closingDropdown, openingDropdown } from "../constants/strategies";
import BodyCard from "./BodyCard";
import { MultiSelect, NumberInput } from "./formInputs";
import LessonCard from "./LessonCard";
import RichTextEditor from "./RichText";

interface Props {
  control: any;
  index: number;
}

const LessonDay = ({ control, index }: Props) => {
  const { getValues, setValue, watch } = useFormContext();
  const body = watch(`lessons.${index}.body`);

  const date = formattedDate(getValues(`lessons.${index}.date`));

  const periods = getValues("period");

  return (
    <div>
      <LessonCard>
        <Title>Opening - {date}</Title>
        <Divider />
        <NumberInput
          control={control}
          defaultValue={periods[0].open_default}
          label="Minutes"
          name={`lessons.${index}.opening.time`}
          placeholder="opening minutes"
        />
        <MultiSelect
          control={control}
          name={`lessons.${index}.opening.strategy`}
          placeholder="select"
          label="select"
          data={openingDropdown}
        />
        <RichTextEditor control={control} label="details" name={`lessons.${index}.opening.details`} />
      </LessonCard>
      {body.map((b, i) => (
        <BodyCard key={i} index={index} k={i} control={control} />
      ))}
      <UnstyledButton
        onClick={() => {
          setValue(`lessons.${index}.body`, [
            ...body,
            {
              time: 1,
              workType: "",
              workTypeDetails: "",
              materials: "",
              strategy: [],
              standards: []
            }
          ]);
        }}
      >
        <Group>
          <PlusIcon />
          <Text>Add Body</Text>
        </Group>
      </UnstyledButton>
      <LessonCard>
        <Title align="center">Closing - {date}</Title>
        <Divider />
        <NumberInput
          control={control}
          defaultValue={periods[0].close_default}
          label="Minutes"
          name={`lessons.${index}.closing.time`}
          placeholder="opening minutes"
        />
        <MultiSelect
          control={control}
          name={`lessons.${index}.closing.strategy`}
          placeholder="select"
          label="select"
          data={closingDropdown}
        />
        <RichTextEditor control={control} label="details" name={`lessons.${index}.closing.details`} />
      </LessonCard>
      {/* <pre>{JSON.stringify(getValues("lessons"), null, 2)}</pre> */}
    </div>
  );
};

export default LessonDay;
