import { yupResolver } from "@hookform/resolvers/yup";
import { Button, Container } from "@mantine/core";
import useLessonCreate from "queries/useLessonCreate";
import { useState } from "react";
import { FormProvider, useForm } from "react-hook-form";
import { getDatesBetweenDates, shortestTime } from "utils/helpers";
import useLeaveConfirm from "utils/routeCancel";
import * as yup from "yup";

import { Step1, Step2 } from "./UnitPlanSteps";
import LessonSummary from "./UnitPlanSteps/Summary";

interface DefaultValues {
  period: any[];
  planName: string;
  date: {
    start: Date | undefined;
    end: Date | undefined;
  };
  subjects: any[];
  careers: any[];
  standardsChosen: any[];
  lessons: any[];
}

interface WizardProps {
  defaultValues: DefaultValues;
}

const checkChange = changed => {
  if (changed.date.start) {
    // handle start date change
  }

  if (changed.date.end) {
    // handle end date change
  }

  if (changed.standardsChosen) {
    // remove standards from lesson bodies
  }

  if (changed.period) {
    // remove standards from lesson bodies
  }
};

const createLessonDays = (dates: Date[], shortestOpening: number, shortestClosing: number) =>
  dates.map(date => ({
    opening: {
      time: shortestOpening,
      strategy: [],
      details: ""
    },
    body: [
      {
        time: 0,
        workType: "",
        workTypeDetails: "",
        materials: "",
        strategy: [],
        standards: []
      }
    ],
    closing: {
      time: shortestClosing,
      strategy: [],
      details: ""
    },
    date
  }));

const getStepContent = step => {
  switch (step) {
    case 0:
      return <Step1 />;
    case 1:
      return <Step2 />;
    case 2:
      return <LessonSummary />;
    default:
      return <div>error not a step</div>;
  }
};

const schema = [
  yup.object().shape({
    period: yup.array().min(1, "Please Select at Least One Period").required("select period"),
    planName: yup.string().required("Please Enter a Plan Name"),
    date: yup.object().shape({
      start: yup.string().required("Please Select a Start Date").nullable(),
      end: yup.string().required("Please Select an End Date").nullable()
    }),
    standardsChosen: yup.array().min(1, "Select One Standard").required("Please Select at Least One Standard")
  }),

  yup.object().shape({
    lessons: yup.array().of(
      yup.object().shape({
        opening: yup.object().shape({
          time: yup.number().required("must have number"),
          strategy: yup.array().min(1, "select a strategy").required("select a strategy"),
          details: yup.string().required("enter detials")
        }),
        body: yup.array().of(
          yup.object().shape({
            time: yup.number().required("time required"),
            workType: yup.string().required("work type required"),
            workTypeDetails: yup.string().required("details required"),
            materials: yup.string().required("materials required"),
            strategy: yup.array().min(1).required("select a strategy"),
            standards: yup.array().min(1).required("select on standard")
          })
        ),
        closing: yup.object().shape({
          time: yup.number().required("must have number"),
          strategy: yup.array().min(1, "select a strategy").required("select a strategy"),
          details: yup.string().required("enter details")
        })
      })
    )
  })
];

const LessonPlanFormWizard = ({ defaultValues }: WizardProps) => {
  const steps = [1, 2];
  const [activeStep, setActiveStep] = useState(0);

  const currentScheme = schema[activeStep] ?? yup.object().shape({});

  const form = useForm({ defaultValues, shouldUnregister: false, resolver: yupResolver(currentScheme) });
  const { handleSubmit, formState, getValues, setValue, trigger } = form;

  const lessonMutation = useLessonCreate();

  useLeaveConfirm({ formState });

  console.log(JSON.stringify(formState.errors, null, 2));
  console.log(JSON.stringify(getValues("lessons.0"), null, 2));

  const onSubmit = async data => {
    const valid = await trigger();

    if (!valid) {
      console.log("not vlaid");
    }

    if (valid) {
      await lessonMutation.submit(data);
      setActiveStep(prev => prev + 1);
    }
  };

  const handleNext = async () => {
    const valid = await trigger();

    if (!valid) {
      return;
    }

    // set next step
    if (!formState.touchedFields.lessons && getValues("lessons").length === 0) {
      const dates = getValues("date");
      const period = getValues("period");
      const start = new Date(dates.start);
      const end = new Date(dates.end);

      const datesArr = getDatesBetweenDates(start, end);

      const shortestOpening = shortestTime(period, "open");
      const shortestClosing = shortestTime(period, "close");

      const lessons = createLessonDays(datesArr, shortestOpening, shortestClosing);

      setValue("lessons", lessons);
    }

    // TODO handle when date change
    // TODO handle when standards change

    setActiveStep(prev => prev + 1);
  };

  const handleBack = () => {
    // set back
    setActiveStep(prev => prev - 1);
  };

  return (
    <div>
      <FormProvider {...form}>
        <Container>
          <form>
            {activeStep < 2 ? (
              <div
                style={{
                  display: "flex",
                  justifyContent: "flex-end",
                  margin: "5px 0"
                }}
              >
                {activeStep > 0 && (
                  <Button onClick={handleBack} disabled={activeStep === 0} mr={8}>
                    back
                  </Button>
                )}
                {activeStep !== steps.length - 1 ? (
                  <Button onClick={handleNext}>Next</Button>
                ) : (
                  <Button disabled={lessonMutation.isLoading} onClick={handleSubmit(onSubmit)}>
                    Submit
                  </Button>
                )}
              </div>
            ) : null}
            {activeStep === 0 || activeStep === 1 ? getStepContent(activeStep) : null}
          </form>
          {activeStep === 2 && getStepContent(activeStep)}
        </Container>
      </FormProvider>
    </div>
  );
};

export default LessonPlanFormWizard;
