import { ActionIcon, Collapse, Group, Paper, Text } from "@mantine/core";
import { ArrowDownIcon, ArrowRightIcon } from "@radix-ui/react-icons";
import React, { useState } from "react";

interface Props {
  children: React.ReactNode;
  title: string;
  selected: boolean;
}

const CollapseContentArea = ({ children, title, selected }: Props) => {
  const [opened, setOpen] = useState(false);
  return (
    <div style={{ margin: "15px 0" }}>
      <Group spacing="xs">
        <Paper
          padding="sm"
          shadow="sm"
          radius="xs"
          sx={theme => ({
            backgroundColor: theme.colors.brand[6],
            // outline: selected ? `${theme.colors.brand[0]} solid 2px` : ""
            border: selected ? `${theme.colors.brand[0]} solid 5px` : ""
          })}
        >
          <Text style={{ color: "white" }}>{title}</Text>
        </Paper>
        <ActionIcon color="dark" size="xl" radius="xl" variant="filled" onClick={() => setOpen(o => !o)}>
          {opened ? <ArrowDownIcon /> : <ArrowRightIcon />}
        </ActionIcon>
      </Group>

      <Collapse in={opened}>{children}</Collapse>
    </div>
  );
};

export default CollapseContentArea;
