import { Group, Navbar, Text } from "@mantine/core";
import { FileTextIcon, HomeIcon, PersonIcon, QuestionMarkIcon } from "@radix-ui/react-icons";
import React from "react";

import NavbarCollapse from "./NavbarCollapse";
import SidebarOption from "./SidebarOption";

const NavbarMenu = ({ setClosed }) => (
  <>
    <Navbar.Section onClick={() => setClosed()}>
      <SidebarOption link="/">
        <Group>
          <HomeIcon />
          <Text>Home</Text>
        </Group>
      </SidebarOption>
    </Navbar.Section>
    <Navbar.Section>
      <NavbarCollapse />
    </Navbar.Section>
    <Navbar.Section onClick={() => setClosed()}>
      <SidebarOption link="/reports">
        <Group>
          <FileTextIcon />
          <Text>Reports</Text>
        </Group>
      </SidebarOption>
    </Navbar.Section>
    <Navbar.Section onClick={() => setClosed()}>
      <SidebarOption link="/myprofile">
        <Group>
          <PersonIcon />
          <Text>My Profile</Text>
        </Group>
      </SidebarOption>
    </Navbar.Section>
    <Navbar.Section onClick={() => setClosed()}>
      <SidebarOption link="/help">
        <Group>
          <QuestionMarkIcon />
          <Text>Help</Text>
        </Group>
      </SidebarOption>
    </Navbar.Section>
  </>
);

export default NavbarMenu;
