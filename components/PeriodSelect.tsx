import { Group, SelectItemProps, Text } from "@mantine/core";
import React from "react";
import { Control, useFormContext } from "react-hook-form";

import { MultiSelect } from "./formInputs";

interface SelectItemPropsExt extends SelectItemProps {
  description: string;
}

const SelectItem = React.forwardRef<HTMLDivElement, SelectItemProps>(
  ({ label, description, ...others }: SelectItemPropsExt, ref) => (
    <div ref={ref} {...others}>
      <Group noWrap>
        <div>
          <Text>{label}</Text>
          <Text size="xs" color="dimmed">
            {description}
          </Text>
        </div>
      </Group>
    </div>
  )
);

interface Dropdown {
  value: string;
  label: string;
  description: string;
}

interface PeriodSelectProps {
  control: Control;
  periodOptions: Dropdown[];
  all: any;
}

const PeriodSelect = ({ control, periodOptions, all }: PeriodSelectProps) => {
  const { setValue, getValues } = useFormContext();

  return (
    <div>
      <MultiSelect
        data={periodOptions}
        name="period"
        label="Period(s)"
        placeholder="Period"
        control={control}
        nothingFound="No options"
        itemComponent={SelectItem}
        defaultValue={getValues("period").map(e => e.period)}
        onChange={value => {
          const obj = value.map(p => {
            const found = all?.find(o => o.period === p);
            return found;
          });

          setValue("period", obj);

          if (obj.length === 0) {
            setValue("subjects", []);
          }
        }}
        value={getValues("period").map(e => e.period)}
      />
    </div>
  );
};

export default PeriodSelect;
