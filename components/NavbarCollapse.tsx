import { Box, Collapse, Group, Text } from "@mantine/core";
import { RulerHorizontalIcon } from "@radix-ui/react-icons";
import { useState } from "react";

import SidebarOption from "./SidebarOption";

const NavbarCollapse = () => {
  const [opened, setOpened] = useState(false);

  const handleCollapse = () => {
    setOpened(o => !o);
  };

  return (
    <>
      <Box
        sx={theme => ({
          display: "block",
          userSelect: "none",
          textAlign: "left",
          padding: theme.spacing.sm,
          borderRadius: theme.radius.md,
          cursor: "pointer",

          "&:hover": {
            backgroundColor: theme.colorScheme === "dark" ? theme.colors.dark[5] : theme.colors.gray[0]
          }
        })}
        onClick={() => handleCollapse()}
      >
        <Group>
          <RulerHorizontalIcon />
          <Text>Lesson Plans</Text>
        </Group>
      </Box>

      <Collapse
        sx={theme => ({
          marginLeft: theme.spacing.xl
        })}
        in={opened}
        transitionDuration={200}
        transitionTimingFunction="linear"
      >
        <SidebarOption callback={() => handleCollapse()} link="/myunitplan/create">
          Create Plan
        </SidebarOption>
        <SidebarOption callback={() => handleCollapse()} link="/myplans">
          My Plans
        </SidebarOption>
        <SidebarOption callback={() => handleCollapse()} link="/myplans">
          Shared Plans
        </SidebarOption>
      </Collapse>
    </>
  );
};

export default NavbarCollapse;
