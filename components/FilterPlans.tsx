import { Button, Center, Grid } from "@mantine/core";
import { Subjects } from "enums";
import React from "react";
import { useForm } from "react-hook-form";

import { DatePicker, MultiSelect, TextInput } from "./formInputs";

interface standardFilterProps {
  grade: string;
  subjects: Array<string>;
  contentArea: string;
  standards: Array<string>;
}

const dropdownTest = [
  {
    value: "0",
    label: "0"
  },
  {
    value: "1",
    label: "1"
  },
  {
    value: "2",
    label: "2"
  },
  {
    value: "3",
    label: "3"
  },
  {
    value: "4",
    label: "4"
  },
  {
    value: "5",
    label: "5"
  }
];

const FilterPlans = () => {
  const form = useForm({
    defaultValues: {
      date: { start: "", end: "" },
      title: [],
      rating: [],
      favorites: [],
      subjects: []
    }
  });

  const { control, handleSubmit } = form;
  const onSubmit = async formData => {
    console.log(formData);
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Grid my={15}>
        <Grid.Col span={2}>
          <DatePicker control={control} name="date.start" label="Start Date" placeholder="Start Date" />
        </Grid.Col>
        <Grid.Col span={2}>
          <DatePicker control={control} name="date.end" label="End Date" placeholder="End Date" />
        </Grid.Col>
        <Grid.Col span={2}>
          <TextInput name="title" control={control} placeholder="Title" label="Title" />
        </Grid.Col>
        <Grid.Col span={2}>
          <MultiSelect data={dropdownTest} name="rating" control={control} placeholder="Rating" label="Rating" />
        </Grid.Col>
        <Grid.Col span={2}>
          <MultiSelect
            data={dropdownTest}
            name="favorites"
            control={control}
            placeholder="Favorites"
            label="Favorites"
          />
        </Grid.Col>
        <Grid.Col span={2}>
          <MultiSelect
            data={Object.keys(Subjects).map(e => ({
              value: Subjects[e],
              label: Subjects[e]
            }))}
            name="subjects"
            control={control}
            placeholder="Subject"
            label="Subject"
          />
        </Grid.Col>
      </Grid>
      <Center my={15}>
        <Button type="submit">Search</Button>
      </Center>
    </form>
  );
};

export default FilterPlans;
