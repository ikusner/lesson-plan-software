import { Grid } from "@mantine/core";
import useFetchInProgress from "queries/useFetchInProgress";

import PlanActionCard from "./PlanActionCard";
import UnitPlanCard from "./UnitPlanCard";

const LessonsInProgress = () => {
  const query = useFetchInProgress();

  console.log(query.data);

  return (
    <Grid>
      <Grid.Col span={12}>
        <PlanActionCard color="#002a5e" content={query?.data?.length ?? 0} title="Lessons in Progress" />
      </Grid.Col>
      {query.isSuccess &&
        query?.data &&
        query?.data.map((lesson, i) => (
          <Grid.Col span={12} key={i}>
            <UnitPlanCard name={lesson?.planName} grade={lesson?.period[0].grade} date={lesson?.date} />
          </Grid.Col>
        ))}
    </Grid>
  );
};

export default LessonsInProgress;
