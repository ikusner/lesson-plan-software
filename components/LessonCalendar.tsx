import { Space, Text } from "@mantine/core";
import { Calendar } from "@mantine/dates";
import { Dispatch, SetStateAction } from "react";

interface CalendarProps {
  callback: Dispatch<SetStateAction<Date>>;
  value: Date;
}

const LessonCalendar = ({ callback, value }: CalendarProps) => {
  const handleChange = date => {
    callback(date);
  };

  return (
    <>
      <Text align="center">View Lessons</Text>
      <Calendar
        fullWidth
        excludeDate={date => date.getDay() === 0 || date.getDay() === 6}
        value={value}
        onChange={handleChange}
        allowLevelChange={false}
      />
      <Space h="lg" />
    </>
  );
};

export default LessonCalendar;
