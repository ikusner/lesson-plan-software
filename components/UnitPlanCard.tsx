import { Card, Text } from "@mantine/core";
import { formattedDate } from "utils/helpers";

interface UnitPlanCardInterface {
  name: string;
  grade: number;
  date: { start: string; end: string };
}

const UnitPlanCard = ({ name, grade, date }: UnitPlanCardInterface) => (
  <div style={{ margin: "auto" }}>
    <Card shadow="lg" padding="lg">
      <Text>{name}</Text>
      <Text>Grade: {grade}</Text>
      <Text>
        {formattedDate(date?.start)} - {formattedDate(date?.end)}
      </Text>
    </Card>
  </div>
);

export default UnitPlanCard;
