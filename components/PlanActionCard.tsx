import { Card, Text } from "@mantine/core";

type color = "#002a5e" | "purple";
interface Props {
  color: color;
  title: string;
  content: string;
}

const PlanActionCard = ({ color, title, content }: Props) => (
  <div style={{ margin: "auto" }}>
    <Card shadow="xl" sx={() => ({ backgroundColor: color })}>
      <Text align="center" style={{ marginBottom: "4rem", color: "white" }}>
        {title}
      </Text>
      <Text align="center" style={{ color: "white" }}>
        {content}
      </Text>
    </Card>
  </div>
);

export default PlanActionCard;
