import { Grid } from "@mantine/core";
import useFetchToRate from "queries/useFetchToRate";

import PlanActionCard from "./PlanActionCard";
import UnitPlanCard from "./UnitPlanCard";

const LessonsToRate = () => {
  const query = useFetchToRate();

  return (
    <Grid>
      <Grid.Col span={12}>
        <PlanActionCard color="#002a5e" content={query?.data?.length ?? 0} title="Lessons to Rate" />
      </Grid.Col>
      {!query.isLoading &&
        query?.data &&
        query?.data.map((lesson, i) => (
          <Grid.Col span={12} key={i}>
            <UnitPlanCard name={lesson?.planName} grade={lesson?.period[0].grade} date={lesson?.date} />
          </Grid.Col>
        ))}
    </Grid>
  );
};

export default LessonsToRate;
