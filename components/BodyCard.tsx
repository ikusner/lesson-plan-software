import { ActionIcon, Chip, Collapse, Divider, Group, Title } from "@mantine/core";
import { ArrowDownIcon, ArrowUpIcon, MinusIcon } from "@radix-ui/react-icons";
import dayjs from "dayjs";
import _ from "lodash";
import { useEffect, useState } from "react";
import { useFormContext } from "react-hook-form";

import { bodyDropdown } from "../constants/strategies";
import { CheckboxChip, Chips, MultiSelect, NumberInput } from "./formInputs";
import LessonCard from "./LessonCard";
import RichTextEditor from "./RichText";

interface Props {
  index: number;
  k: number;
  control: any;
}

const BodyCard = ({ index, k, control }: Props) => {
  const { getValues, watch, setValue } = useFormContext();
  const [opened, setOpened] = useState(true);

  const workType = watch(`lessons.${index}.body.${k}.workType`);

  const date = dayjs(getValues(`lessons.${index}.date`)).format("MM/DD/YYYY");

  const periods = getValues("period");

  useEffect(() => {
    setOpened(true);
  }, [index]);

  return (
    <LessonCard>
      <Group position="apart">
        <ActionIcon onClick={() => setOpened(p => !p)}>{opened ? <ArrowDownIcon /> : <ArrowUpIcon />}</ActionIcon>
        <Title align="center">
          Body of Lesson {k + 1} - {date}
        </Title>
        <ActionIcon
          onClick={() => {
            const bodiesClone = _.cloneDeep([...getValues(`lessons.${index}.body`)]);

            bodiesClone.splice(k, 1);
            setValue(`lessons.${index}.body`, bodiesClone);
          }}
          style={k === 0 ? { visibility: "hidden" } : {}}
        >
          <MinusIcon />
        </ActionIcon>
      </Group>
      <Divider />
      <Collapse in={opened}>
        <NumberInput
          control={control}
          defaultValue={0}
          label="Minutes"
          name={`lessons.${index}.body.${k}.time`}
          placeholder="opening minutes"
        />
        <Chips
          control={control}
          id={`lessons.${index}.body.${k}.workType`}
          label="work type"
          name={`lessons.${index}.body.${k}.workType`}
        >
          <Chip value="individual">Individual</Chip>
          <Chip value="small">Small</Chip>
          <Chip value="whole">Whole</Chip>
        </Chips>
        {workType && (
          <RichTextEditor
            control={control}
            label="work type details"
            name={`lessons.${index}.body.${k}.workTypeDetails`}
          />
        )}
        <RichTextEditor name={`lessons.${index}.body.${k}.materials`} control={control} label="materials" />
        <MultiSelect
          control={control}
          name={`lessons.${index}.body.${k}.strategy`}
          data={bodyDropdown}
          label="strategy"
          placeholder="strategy"
        />
        <CheckboxChip
          control={control}
          name={`lessons.${index}.body.${k}.standards`}
          label="Standards for This Body of Lesson"
        />
      </Collapse>
    </LessonCard>
  );
};
export default BodyCard;
