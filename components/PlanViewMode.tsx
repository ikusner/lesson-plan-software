import { Button, Container } from "@mantine/core";
import { useState } from "react";
import { FormProvider, useForm } from "react-hook-form";
import { getDatesBetweenDates, shortestTime } from "utils/helpers";

import { Step1, Step2 } from "./UnitPlanSteps";
import LessonSummary from "./UnitPlanSteps/Summary";

interface DefaultValues {
  period: any[];
  planName: string;
  date: {
    start: Date | undefined;
    end: Date | undefined;
  };
  subjects: any[];
  careers: any[];
  standardsChosen: any[];
  lessons: any[];
}

interface WizardProps {
  defaultValues: DefaultValues;
}

const getStepContent = step => {
  switch (step) {
    case 0:
      return <Step1 />;
    case 1:
      return <Step2 />;
    case 2:
      return <LessonSummary />;
    default:
      return <div>error not a step</div>;
  }
};

const PlanViewMode = ({ defaultValues }: WizardProps) => {
  const steps = [1, 2];
  const [activeStep, setActiveStep] = useState(0);
  const form = useForm({ defaultValues, shouldUnregister: false });
  const { handleSubmit, formState, getValues, setValue } = form;

  const onSubmit = () => {
    setActiveStep(prev => prev + 1);
  };

  const handleNext = async () => {
    setActiveStep(prev => prev + 1);
  };

  const handleBack = () => {
    // set back
    setActiveStep(prev => prev - 1);
  };

  return (
    <div>
      <FormProvider {...form}>
        <Container>
          <form>
            {activeStep < 2 ? (
              <div
                style={{
                  display: "flex",
                  justifyContent: "flex-end",
                  margin: "5px 0"
                }}
              >
                {activeStep > 0 && (
                  <Button onClick={handleBack} disabled={activeStep === 0} mr={8}>
                    back
                  </Button>
                )}
                {activeStep !== steps.length - 1 ? (
                  <Button onClick={handleNext}>Next</Button>
                ) : (
                  <Button onClick={handleSubmit(onSubmit)}>Submit</Button>
                )}
              </div>
            ) : null}
            {activeStep === 0 || activeStep === 1 ? getStepContent(activeStep) : null}
          </form>
          {activeStep === 2 && getStepContent(activeStep)}
        </Container>
      </FormProvider>
    </div>
  );
};

export default PlanViewMode;
