import { Card, Grid, Text } from "@mantine/core";
import Link from "next/link";
import React from "react";

interface Props {
  data: any[];
}

const MyPlansCards = ({ data }: Props) => (
  <Grid>
    {data.map((el, i) => (
      <Grid.Col span={6} key={i}>
        <Link href={`/myplans/${i}`} passHref>
          <Card shadow="lg" padding="lg" component="a">
            <Text>Plan Dates: {el.planDate}</Text>
            <Text>Title: {el.title}</Text>
            <Text>
              Rating:
              {
                // eslint-disable-next-line no-unused-vars
                [...Array(el.rating as number)].map(_e => (
                  <span style={{ color: "#002a5e" }}>&#9733;</span>
                ))
              }
            </Text>
          </Card>
        </Link>
      </Grid.Col>
    ))}
  </Grid>
);

export default MyPlansCards;
