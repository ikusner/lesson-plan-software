import { Card } from "@mantine/core";

const LessonCard = ({ children }) => (
  <div style={{ margin: "auto", marginTop: "10px" }}>
    <Card shadow="sm" padding="lg" withBorder style={{ overflow: "visible" }}>
      {children}
    </Card>
  </div>
);

export default LessonCard;
