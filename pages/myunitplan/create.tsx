// import { useRouter } from "next/router";
import React, { ReactElement, useState } from "react";

import SiteLayout from "../../components/layouts/SiteLayout";
import LessonPlanFormWizard from "../../components/LessonPlanFormWizard";

const defaultValues = {
  period: [],
  planName: "",
  date: {
    start: null,
    end: null
  },
  subjects: [],
  careers: [],
  standardsChosen: [],
  lessons: [],
  finished: false
};

// eslint-disable-next-line arrow-body-style
const Create = () => {
  // const router = useRouter();
  // const { id } = router.query;
  // const newLesson = router.query.new;

  return <LessonPlanFormWizard defaultValues={defaultValues} />;
};

Create.getLayout = (page: ReactElement) => <SiteLayout head="Create New Plan">{page}</SiteLayout>;

export default Create;
