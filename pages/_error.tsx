import ErrorLayout from "@components/layouts/ErrorLayout";
import { ReactElement } from "react";

const Error = ({ statusCode }) => (
  <p>{statusCode ? `An error ${statusCode} occurred on server` : "An error occurred on client"}</p>
);

Error.getInitialProps = ({ res, err }) => {
  // eslint-disable-next-line no-nested-ternary
  const statusCode = res ? res.statusCode : err ? err.statusCode : 404;
  return { statusCode };
};

Error.getLayout = (page: ReactElement) => <ErrorLayout head="Error">{page}</ErrorLayout>;

export default Error;
