import { NextApiRequest, NextApiResponse } from "next";

import { sampleStandardsData } from "../../../utils/sample-data";

// eslint-disable-next-line consistent-return
const handler = (_req: NextApiRequest, res: NextApiResponse) => {
  try {
    const { body } = _req;

    if (body) {
      const toReturn = body.map(key => sampleStandardsData[key]);
      return res.status(200).json(toReturn);
    }

    throw Error("no body");
  } catch (err) {
    res.status(500).json({ statusCode: 500, message: err.message });
  }
};

export default handler;
