import { NextApiRequest, NextApiResponse } from "next";

import { sampleLessonData } from "../../../utils/sample-data";

// eslint-disable-next-line consistent-return
const handler = (_req: NextApiRequest, res: NextApiResponse) => {
  const { id } = _req.query;

  try {
    if (!Array.isArray(sampleLessonData)) {
      throw new Error("Cannot find user data");
    }

    if (typeof id === "string") {
      const lesson = sampleLessonData[id];
      if (!lesson) {
        throw new Error("cannot find id");
      }
      return res.status(200).json(sampleLessonData[0]);
    }

    throw new Error("param not type string");
  } catch (err) {
    res.status(500).json({ statusCode: 500, message: err.message });
  }
};

export default handler;
