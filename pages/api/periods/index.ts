import { NextApiRequest, NextApiResponse } from "next";

import { samplePeriodsData } from "../../../utils/sample-data";

const handler = (_req: NextApiRequest, res: NextApiResponse) => {
  try {
    if (!Array.isArray(samplePeriodsData)) {
      throw new Error("Cannot find user data");
    }

    res.status(200).json(samplePeriodsData);
  } catch (err) {
    res.status(500).json({ statusCode: 500, message: err.message });
  }
};

export default handler;
