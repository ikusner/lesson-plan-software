import { NextApiRequest, NextApiResponse } from "next";

import { sampleReportData } from "../../../utils/sample-data";

// todo: add filter for date. so add date range to request body
const handler = (_req: NextApiRequest, res: NextApiResponse) => {
  try {
    if (!Array.isArray(sampleReportData)) {
      throw new Error("Cannot find user data");
    }

    res.status(200).json(sampleReportData);
  } catch (err) {
    res.status(500).json({ statusCode: 500, message: err.message });
  }
};

export default handler;
