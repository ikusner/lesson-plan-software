import { NextApiRequest, NextApiResponse } from "next";

import { sampleReportData } from "../../../utils/sample-data";

const datesAreOnSameDay = (first, second) =>
  first.getFullYear() === second.getFullYear() &&
  first.getMonth() === second.getMonth() &&
  first.getDate() === second.getDate();

// eslint-disable-next-line consistent-return
const handler = (_req: NextApiRequest, res: NextApiResponse) => {
  const { date } = _req.query;

  try {
    if (!Array.isArray(sampleReportData)) {
      throw new Error("Cannot find user data");
    }

    if (typeof date === "string") {
      const reports = sampleReportData.filter(d => datesAreOnSameDay(d.date, new Date(date)));

      if (!reports) {
        throw new Error("cannot find date");
      }
      return res.status(200).json(reports);
    }

    throw new Error("param not type string");
  } catch (err) {
    res.status(500).json({ statusCode: 500, message: err.message });
  }
};

export default handler;
