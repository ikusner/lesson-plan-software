import PlanViewMode from "@components/PlanViewMode";
import { useRouter } from "next/router";
import useFetchUnitPlan from "queries/useFetchUnitPlan";
import React, { ReactElement, useState } from "react";

import SiteLayout from "../../components/layouts/SiteLayout";
import LessonPlanFormWizard from "../../components/LessonPlanFormWizard";

const defaultValues = {
  period: [],
  planName: "",
  date: {
    start: null,
    end: null
  },
  subjects: [],
  careers: [],
  standardsChosen: [],
  lessons: [],
  finished: false
};

const Create = () => {
  const router = useRouter();
  const { id } = router.query;

  const query = useFetchUnitPlan(id);

  console.log("LESSON", query.data);

  return !query.isLoading && query?.data ? <LessonPlanFormWizard defaultValues={query?.data} /> : null;
};

export default Create;

Create.getLayout = (page: ReactElement) => <SiteLayout head="Create New Plan">{page}</SiteLayout>;
