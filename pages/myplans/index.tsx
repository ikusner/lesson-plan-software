import FilterPlans from "@components/FilterPlans";
import FilterPlansStandards from "@components/FilterPlansStandards";
import MyPlansCards from "@components/MyPlansCards";
import { Button, Container, Divider, Group, Text } from "@mantine/core";
import React, { ReactElement } from "react";

import SiteLayout from "../../components/layouts/SiteLayout";

const MyPlans = () => {
  const data = React.useMemo(
    () => [
      { planDate: "10/1", title: "Egyptian", rating: 3 },
      { planDate: "10/1", title: "Fractions", rating: 1 },
      { planDate: "10/8", title: "Algerbra", rating: 2 },
      { planDate: "10/9", title: "Europe", rating: 1 }
    ],
    []
  );

  return (
    <Container fluid>
      <FilterPlans />
      <Divider />
      <FilterPlansStandards />
      <Divider />
      <Group position="center" style={{ margin: "15px 0" }}>
        <Button size="xl">Social Studies Plans</Button>
        <Button size="xl">Math Plans</Button>
        <Button size="xl">Art Plans</Button>
      </Group>
      <Container>
        <Text my={15}>Search Results</Text>
        <MyPlansCards data={data} />
      </Container>
    </Container>
  );
};

MyPlans.getLayout = (page: ReactElement) => <SiteLayout head="My Plans">{page}</SiteLayout>;

export default MyPlans;
