import "../printstyles.css";

import { MantineProvider } from "@mantine/core";
import { NextPage } from "next";
import { AppProps } from "next/app";
import { ReactElement, ReactNode, useState } from "react";
import { QueryClient, QueryClientProvider } from "react-query";
import { ReactQueryDevtools } from "react-query/devtools";

type NextPageWithLayout = NextPage & {
  getLayout?: (page: ReactElement) => ReactNode;
};

type AppPropsWithLayout = AppProps & {
  Component: NextPageWithLayout;
};

const App = (props: AppPropsWithLayout) => {
  const { Component, pageProps } = props;

  const getLayout = Component.getLayout ?? (page => page);

  const pageWithLayout = getLayout(<Component {...pageProps} />);

  const [queryClient] = useState(() => new QueryClient());

  return (
    <MantineProvider
      withGlobalStyles
      withNormalizeCSS
      theme={{
        /** Put your mantine theme override here */
        colorScheme: "light",
        colors: {
          brand: [
            "#3D5068",
            "#324963",
            "#284160",
            "#1E3B5E",
            "#14355C",
            "#0A2F5C",
            "#002A5D",
            "#08274B",
            "#0E233E",
            "#112033"
          ],

          brand2: [
            "#3872B9",
            "#2A61A4",
            "#1E5292",
            "#144582",
            "#0C3B74",
            "#053268",
            "#002A5E",
            "#04234A",
            "#061E3B",
            "#07192E"
          ]
        },
        primaryColor: "brand2"
      }}
    >
      <QueryClientProvider client={queryClient}>
        {pageWithLayout}
        <ReactQueryDevtools initialIsOpen={false} />
      </QueryClientProvider>
    </MantineProvider>
  );
};

export default App;
