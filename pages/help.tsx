import { ReactElement } from "react";

import SiteLayout from "../components/layouts/SiteLayout";

const Help = () => (
  <div>
    <div>HELP ME</div>
  </div>
);

Help.getLayout = (page: ReactElement) => <SiteLayout head="Help">{page}</SiteLayout>;

export default Help;
