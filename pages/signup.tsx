import NoAuthLayout from "@components/layouts/NoAuthLayout";
import { PasswordInput, TextInput } from "@mantine/core";
import React, { ReactElement } from "react";

const SignUp = () => (
  <div>
    <TextInput />
    <PasswordInput />
  </div>
);

SignUp.getLayout = (page: ReactElement) => <NoAuthLayout head="Sign Up">{page}</NoAuthLayout>;

export default SignUp;
