import LessonsInProgress from "@components/LessonsInProgress";
import LessonsToRate from "@components/LessonsToRate";
import { Container, Grid } from "@mantine/core";
import { ReactElement } from "react";

import Chart from "../components/Chart";
import SiteLayout from "../components/layouts/SiteLayout";
import LessonView from "../components/LessonView";

const IndexPage = () => (
  <Container>
    <Grid>
      <Grid.Col span={12} style={{ height: 300 }}>
        <Chart />
      </Grid.Col>
      <Grid.Col span={8}>
        <Grid gutter="sm">
          <Grid.Col span={6}>
            <LessonsInProgress />
          </Grid.Col>
          <Grid.Col span={6}>
            <LessonsToRate />
          </Grid.Col>
        </Grid>
      </Grid.Col>
      <Grid.Col span={4}>
        <LessonView />
      </Grid.Col>
    </Grid>
  </Container>
);

IndexPage.getLayout = (page: ReactElement) => <SiteLayout head="Home Page">{page}</SiteLayout>;

export default IndexPage;
